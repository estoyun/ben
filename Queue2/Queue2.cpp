#include <stdio.h>
#include <string.h>

#define TRUE 1
#define FALSE 0

//배열 기반 원형 큐로 구현
#define MAX_SIZE 1000
typedef int Data;

typedef struct _queue { //큐 구조체 정의
	Data arr[MAX_SIZE];
	int front;
	int rear;
	int size;
}Queue;

void InitQueue(Queue * pq) { //큐 초기화
	pq->size = 0;
	pq->front = 0;
	pq->rear = 0;
}
int getNextIdx(int idx) { //다음 인덱스 get
	if (idx + 1 >= MAX_SIZE) return 0;
	return idx + 1;
}
void setNextIdx(int *idx) { //다음 인덱스 set
	if (*idx + 1 >= MAX_SIZE) *idx = 0;
	else (*idx)++;
}

int Empty(Queue * pq) {
	if (pq->front == pq->rear) return TRUE;
	return FALSE;
}
int Full(Queue *pq) {
	if (getNextIdx(pq->rear) == pq->front) return TRUE;
	return FALSE;
}

Data Front(Queue *pq) {
	if (Empty(pq) == TRUE) return -1;
	return pq->arr[pq->front];
}

Data Back(Queue *pq) {
	if (Empty(pq) == TRUE) return -1;
	return pq->arr[pq->rear - 1];
}

void Push(Queue *pq, Data data) {
	if (Full(pq) == TRUE) return;
	pq->arr[pq->rear] = data;
	setNextIdx(&(pq->rear));
	(pq->size)++;
}

Data Pop(Queue *pq) {
	if (Empty(pq) == TRUE) return -1;

	int data = pq->arr[pq->front];
	setNextIdx(&(pq->front));
	(pq->size)--;
	return data;
}

int n, m, v; 
int graph[1001][1001]; //그래프
int vis[1001]; //visit 했는지 여부 체크하기 위한 배열

void dfs(int cur) //깊이 우선 탐색
{
	printf("%d ", cur);
	vis[cur] = 1; //방문 했음
	for (int i = 1; i <= n; i++) {
		if (graph[cur][i] && vis[i] == 0) { //visit 하지 않은 것만 출력합시다!
			dfs(i); //재귀 시작하세용!
		}
	}
}

void bfs(int src) //너비 우선 탐색
{
	Queue q;
	InitQueue(&q);
	Data data = src;

	Push(&q, data);
	vis[data] = 2; //dfs와 구분 짓기 위해서
	while (!Empty(&q)) {
		int cur = Front(&q);
		printf("%d ", cur);
		Pop(&q);
		for (int i = 1; i <= n; i++) {
			if (graph[cur][i] && vis[i] != 2) { //visit 하지 않은 것만 출력합시다!
				vis[i] = 2;
				Push(&q, i);
			}
		}
	}
}

int main()
{
	scanf("%d%d%d", &n, &m, &v); //n : 정점의 개수(노드), m : 간선의 개수(선), v : 탐색 시작 정점 번호(시작 노드)

	for (int i = 0; i<m; i++) {
		int x, y;
		scanf("%d%d", &x, &y);
		graph[x][y] = graph[y][x] = 1; //간선 잇기
	}

	dfs(v); //깊이 우선 탐색

	puts("");

	bfs(v); //너비 우선 탐색
}