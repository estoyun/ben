#include <iostream>

using namespace std;


long long Fibo(int input) //피보나치 계산
{

	long long first_value = 0;
	long long second_value = 1;
	long long cal = 0;


	for (int i = 1; i < input; i++)
	{
		cal = first_value + second_value;
		first_value = second_value;
		second_value = cal;
	}

	return second_value;
}

int main()
{
	//입력부
	int input;
	cin >> input;

	//출력부
	cout << Fibo(input);
}