#include <iostream>

using namespace std;

void Fibo(int input) //피보나치 계산
{

	int first_value = 0;
	int second_value = 1;
	int cal = 0;



	for (int i = 1; i < input; i++)
	{
		cal = (first_value + second_value);
		first_value = second_value;
		second_value = cal;
	}

	if (input == 0)
	{
		first_value = 1;
		second_value = 0;
	}
	cout << first_value << " " << second_value << "\n";

}

int main()
{
	int t, temp = 0;
	cin >> t;

	for (int i = 0; i < t; i++)
	{
		cin >> temp;
		Fibo(temp);
	}
	return 0;
}