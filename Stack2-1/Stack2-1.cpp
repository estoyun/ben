#include <iostream>
#include <vector>
#include <stack>

using namespace std;

int main()
{
	int n;
	cin >> n;
	stack<int> st;

	// push 와 pop은 2n번 사용됨
	vector<char> ret(2 * n);

	// stack에 push될 다음 인자
	int next = 1;
	int counter = 0;

	for (int i = 0; i < n; i++) {
		int cur;

		scanf("%d", &cur);

		while (st.empty() || cur != st.top()) {
			if (next > n) {  // next가 n보다 커지면 불가능함
				printf("NO\n");
				return 0;
			}

			st.push(next++);
			ret[counter++] = '+';
		}
		st.pop();
		ret[counter++] = '-';
	}

	for (int i = 0; i < ret.size(); i++)
		printf("%c\n", ret[i]);
}