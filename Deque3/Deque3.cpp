#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <typeinfo>
#include <string.h>

using namespace std;

#define TRUE 1
#define FALSE 0

int main(void)
{
	//입력부

	int t;
	cin >> t;

	for (int i = 0; i < t; i++)
	{
		int n;
		string func;
		deque<int> dq;
		char value[400002];
		bool isError = FALSE;
		bool isReverse = FALSE;

		int comma_count = 0;

		cin >> func >> n >> value;
		dq.clear();

		string temp = "";
		char *token;
		token = strtok(value, "[,]");
		if (token != NULL)
			dq.push_back(atoi(token));
		while (token = strtok(NULL, "[,]")) //문자열 끝까지
		{
			dq.push_back(atoi(token));
		}


		//처리부
		for (int k = 0; k < func.size(); k++)
		{
			if (func[k] == 'R') //R
			{
				isReverse = !isReverse;
			}

			else //D
			{
				if (dq.empty())
				{
					isError = TRUE;
					break;
				}

				if (isReverse == FALSE) //R 안되었을 때
					dq.pop_front();
				else //R 되었을 때
					dq.pop_back();
			}
		}

		//출력부
		if (isError) //에러
		{
			cout << "error\n";
		}
		else //정상
		{
			cout << "[";

			for (int l = 0; l < dq.size(); l++) //출력
			{
				if (isReverse == FALSE)
					cout << dq[l];
				else
					cout << dq[dq.size() - l - 1];
				if (!(l == dq.size() - 1))
					cout << ",";
			}

			cout << "]\n";

		}


	

	}
	return 0;
}