#include <iostream>
#include <vector>
#include <deque>
#include<math.h>

using namespace std;

int main()
{
	deque<int> dq;

	int n, m; 
	cin >> n >> m; //뽑아내려하는 수의 개수 입력

	for (int i = 1; i <= n; i++) { //deque에 순서대로 집어 넣기
		dq.push_back(i);
	}


	int count = 0; //result count

	for (int i = 0; i < m; i++) {
		int input;
		cin >> input;
		int index;

		for (int j = 0; j < dq.size(); j++) {
			if (dq[j] == input)
			{
				index = j;
				break;
			}

				
		}
		while (dq.front() != input)
		{
			if (index<=(dq.size()/2)) { //2번
				dq.push_back(dq.front());
				dq.pop_front();

			}

			else {//3번
				dq.push_front(dq.back());
				dq.pop_back();
			}
			count++;

		}
			dq.pop_front();
	}
	cout << count;
}