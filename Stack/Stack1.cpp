#include <stdio.h>
#include <string.h>

#define TRUE 1         //반환 값 1
#define FALSE 0        //반환 값 0
#define MINUS -1       //반환 값 -1

#define MAX_SIZE 10000  //10000개의 명령까지!

int printArr[10000]; //출력할 값들 저장
int count = 0; //위 배열 인덱스

typedef struct _stack  //스택 구조체
{
	int arr[MAX_SIZE];
	int top;
}Stack;

void StackInit(Stack * sp)   //스택 초기화 함수
{
	sp->top = -1;
}

int IsEmpty(Stack * sp)    //스택 비어있는지 확인
{
	if (sp->top == -1)
		return TRUE;
	return FALSE;
}

int Size(Stack * sp)  //스택의 현재 size 반환
{
	return sp->top + 1;
}

int IsFull(Stack * sp) //스택이 꽉 차있는지 확인
{
	if (sp->top + 1 >= MAX_SIZE) return TRUE;
	return FALSE;
}

void Push(Stack * sp, int data) //스택의 push
{
	if (IsFull(sp) == TRUE)
		return;
	
	//top 한 개 올리고 data 삽입!
	sp->arr[++(sp->top)] = data;
}

int Pop(Stack * sp) //스택의 pop
{
	if (IsEmpty(sp) == TRUE) return MINUS;

	//top 한 개 줄여버리기(data pop)
	return sp->arr[(sp->top)--];
}

int Peek(Stack * sp) //스택의 맨 위 인자(index) 반환
{
	if (IsEmpty(sp) == TRUE) return MINUS;
	return sp->arr[sp->top];
}

int main()
{
	int n, num;
	char str[6];
	Stack stack;
	int i;

	scanf("%d", &n); //입력 횟수 받기
	fgetc(stdin); //버퍼 비우기
	StackInit(&stack); //스택 초기화

	for (i = 0; i < MAX_SIZE; i++) //출력 배열 -2로 초기화
		printArr[i] = -2;

	for (i = 0; i < n; i++)
	{
		scanf("%s", str);
		fgetc(stdin); //버퍼 비우기

		if (!strcmp(str, "push")) //push 인 경우
		{
			scanf("%d", &num); //스택에 집어 넣을 숫자 입력
			fgetc(stdin); //버퍼 비우기
			Push(&stack, num);
		}

		else if (!strcmp(str, "pop")) //pop인 경우
		{
			printArr[count++] = Pop(&stack);
		}

		else if (!strcmp(str, "empty")) //empty인 경우
		{
			printArr[count++] = IsEmpty(&stack);
		}

		else if (!strcmp(str, "size")) //size인 경우
		{
			printArr[count++] = Size(&stack);
		}

		else if (!strcmp(str, "top")) //top인 경우
		{
			printArr[count++] = Peek(&stack);
		}
	}

	for (i = 0; i < MAX_SIZE; i++) //출력 배열 출력하기!
	{
		if (!(printArr[i] == -2)) //초기 값이 아니면 출력
			printf("%d\n", printArr[i]);
		else
			break;
	}
	return 0;
}