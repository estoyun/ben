#include <stdio.h>
#pragma warning (disable:4996)

int stack[100000];
int res[200000];

int main()
{
	int i, n, k, stkIdx = 0, resIdx = 0, max = 0;
	scanf("%d", &n);

	while (n--)
	{
		scanf("%d", &k);

		if(k > max)
			for (i = max; i < k; i++)
			{
				stack[stkIdx++] = i + 1;
				res[resIdx++] = 1;
				printf("+\n");
			}

		else
		{
			if (stack[stkIdx-1] != k)
				puts("NO"); return 0;
		}
		stkIdx--;
		stack[stkIdx] = 0;
		res[resIdx++] = 0;
		printf("-\n");
		if (max < k) max = k;
	}

	for (i = 0; i < resIdx; i++)
	{
		if (res[i] == 1)
			printf("+\n");
		else if(res[i] == 0)
			printf("-\n");

	}
	return 0;
}