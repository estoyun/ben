#include <iostream>
#include <string>
#include <stack>

using namespace std;

int main() {
		stack<char> st;

		string s;

		cin >> s;

		if (s.length() > 30)
		{
			cout << 0;
			return 0;
		}

		int ans = 0, k = 0, l = 0, mul = 1;

		for (int i = 0; i<s.length(); i++)
		{
			char now = s[i];
			switch (now) {
			case '(':
				++k;
				st.push(now);
				mul *= 2;
				if (i + 1<s.length() && s[i + 1] == ')')
					ans += mul;
				break;

			case '[':
				++l;
				st.push(now);
				mul *= 3;
				if (i + 1<s.length() && s[i + 1] == ']')
					ans += mul;
				break;

			case ')':
				--k;

				if (st.empty())
				{
					cout << 0;
					return 0;
				}

				st.pop();
				mul /= 2;
				break;

			case ']':
				--l;

				if (st.empty())
				{
					cout << 0;
					return 0;
				}

				st.pop();
				mul /= 3;
				break;
			}
		}

		if (st.size() != 0 || l != 0 || k != 0)
			cout << 0;
		else cout << ans;

	return 0;
}