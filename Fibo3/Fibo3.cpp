#include <iostream>

using namespace std;


int Fibo(int input) //피보나치 계산
{

	int first_value = 0;
	int second_value = 1;
	int cal = 0;


	for (int i = 1; i < input; i++)
	{
		cal = (first_value + second_value) % 1000000;
		first_value = second_value;
		second_value = cal;
	}

	return second_value % 1000000;
}

int main()
{
	//입력부
	long long input;
	cin >> input; 

	//출력부
	cout << Fibo(input% 1500000);
}