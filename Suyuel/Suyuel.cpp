#include <stdio.h>
#include <string.h>
#include <math.h>

int flag = 0;
char currentNum[81]; //입력 받는 숫자에 따른 문자열 길이
char num[3] = { '1', '2', '3' }; //1,2,3 char화
char temp[41]; //반으로 나눈 것 저장
int result;
int N;

int promising(int length);
void backtrack(int current);

void main()
{
	int i;

	scanf("%d", &N);

	for (i = 0; i<3; i++)
	{
		currentNum[0] = num[i];
		backtrack(0);
	}
}

void backtrack(int current)
{
	int i;
	int resultnum = 0;

	//promising 한지 아닌지 본다
	if (promising(current + 1) == 0 || flag == 1)
	{
		return;
	}

	if (current == N - 1) //길이가 N인 좋은 수열이 만들어짐
	{
		for (i = 0; i <= N - 1; i++)
			printf("%d", currentNum[i] - '0');

		flag++;

		return;

	}

	for (i = 0; i<3; i++)
	{
		currentNum[current + 1] = num[i];

		backtrack(current + 1);

		currentNum[current + 1] = ' ';
	}
}

int promising(int length)
{
	int n = length / 2;
	int i, j, k;
	int cnt;

	strcpy(temp, " "); //공백을 temp에 복사

	for (i = 1; i <= n; i++)
	{
		for (k = 0; k + i <= length; k = k + i)
		{
			for (j = 0; j<i; j++)
			{
				temp[j] = currentNum[k + j]; //길이가 i(i<=N/2)인 부분 수열이 있는지 보려고 temp에 저장
			}

			cnt = 0;

			for (j = 0; j<i; j++)
			{
				if (temp[j] == currentNum[k + i + j])
					cnt++;
			}

			if (cnt == i) {
				return 0; //부분 수열이 존재
			}
		}
	}
	return 1;
}