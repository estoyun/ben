#include<stdio.h>
#include<string.h>

#define TRUE 1
#define FALSE 0
#define MINUS -1       //반환 값 -1

//배열 기반 원형 큐로 구현
#define MAX_SIZE 25000 

typedef int Data;

typedef struct _dequeue{ //큐 구조체 정의
	Data arr[MAX_SIZE];
	int front;
	int rear;
	int size;
}Dequeue;

void InitDequeue(Dequeue * pq) { //큐 초기화
	pq->size = 0;
	pq->front = 10001;
	pq->rear = 10000;
}

int Empty(Dequeue * pq) {
	if (pq->front >  pq->rear)
		return TRUE;
	return FALSE;
}

Data Front(Dequeue *pq) {
	if (Empty(pq) == TRUE)
		return -1;
	return pq->arr[pq->front];
}

Data Back(Dequeue *pq) {
	if (Empty(pq) == TRUE)
		return -1;
	return pq->arr[pq->rear];
}

void push_front(Dequeue *pq, Data data) {
	pq->arr[--(pq->front)] = data;
	(pq->size)++;
}

Data pop_front(Dequeue *pq) {
	if (Empty(pq) == TRUE)
		return -1;

	int data = pq->arr[(pq->front)++];
	(pq->size)--;
	return data;
}

void push_back(Dequeue * pq, int data) //스택의 push
{
	//rear 한 개 올리고 data 삽입!
	pq->arr[++(pq->rear)] = data;
	(pq->size)++;
}

int pop_back(Dequeue * pq) //스택의 pop
{	
	if (Empty(pq) == TRUE)
		return -1;
	//rear 한 개 줄여버리기(data pop)
	int data = pq->arr[(pq->rear)--];
	(pq->size)--;
	return data;
}


int main(void)
{
	int i, N;
	char str[30];

	Dequeue q;
	InitDequeue(&q);
	Data data;

	scanf("%d", &N);

	for (i = 0; i<N; i++) {
		scanf("%s", str);

		if (!strcmp(str, "push_back")) {
			scanf("%d", &data);
			push_back(&q, data);

		}
		else if (!strcmp(str, "push_front")) {
			scanf("%d", &data);
			push_front(&q, data);
		}

		else if (!strcmp(str, "pop_back")) {
			printf("%d\n", pop_back(&q));

		}

		else if (!strcmp(str, "pop_front")) {
			printf("%d\n", pop_front(&q));

		}

		else if (!strcmp(str, "size")) {
			printf("%d\n", q.size);

		}
		else if (!strcmp(str, "empty")) {
			printf("%d\n", Empty(&q));

		}
		else if (!strcmp(str, "front")) {
			printf("%d\n", Front(&q));

		}
		else if (!strcmp(str, "back")) {
			printf("%d\n", Back(&q));

		}

	}

	return 0;
}