#include <iostream>

using namespace std;

int arr[100]; //피보나치 수 배열


int Fibo(int input) //피보나치 계산
{

	arr[0] = 0;
	arr[1] = 1;

	for (int i = 2; i <= input; i++)
	{
		arr[i] = arr[i - 2] + arr[i - 1];
	}

	return arr[input];
}

int main()
{
	//입력부
	int input;
	cin >> input;

	//출력부
	cout << Fibo(input);
}